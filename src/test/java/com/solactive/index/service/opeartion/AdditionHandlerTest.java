package com.solactive.index.service.opeartion;

import com.solactive.index.dto.AdditionOperation;
import com.solactive.index.dto.OperationType;
import com.solactive.index.entity.IndexEntity;
import com.solactive.index.entity.ShareEntity;
import com.solactive.index.exception.IndexException;
import com.solactive.index.service.operation.AdditionHandler;
import com.solactive.index.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AdditionHandlerTest {
    private final AdditionHandler additionHandler = new AdditionHandler();

    private IndexEntity indexEntity;

    @BeforeEach
    public void setUp() {
        List<ShareEntity> shares = new ArrayList<>();
        shares.add(new ShareEntity("A.OQ", 10.0, 20.0, 0, 0));
        indexEntity = new IndexEntity("INDEX_1", shares, 1000.0);
    }

    @Test
    public void testExecuteSuccessfulAddition() {
        AdditionOperation additionOperation = TestUtil.additionOperation("INDEX_1", "B.OQ", 3, 1);

        IndexEntity result = additionHandler.execute(indexEntity, additionOperation);

        assertNotNull(result);
        assertEquals(2, result.getIndexShares().size());
        assertTrue(result.getIndexShares().stream().anyMatch(s -> s.getShareName().equals("B.OQ")));
    }

    @Test
    public void testExecuteShareAlreadyExists() {
        AdditionOperation additionOperation = TestUtil.additionOperation("INDEX_1", "A.OQ", 3, 1);
        IndexException exception = assertThrows(IndexException.class, () -> {
            additionHandler.execute(indexEntity, additionOperation);
        });
        assertEquals(HttpStatus.ACCEPTED, exception.getStatusCode());
        assertEquals("202 ACCEPTED \"Share Already Exists\"", exception.getMessage());
    }

    @Test
    public void testType() {
        assertEquals(OperationType.ADDITION, additionHandler.type());
    }
}
