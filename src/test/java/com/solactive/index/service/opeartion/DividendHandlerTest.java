package com.solactive.index.service.opeartion;


import com.solactive.index.dto.DividendOperation;
import com.solactive.index.dto.OperationType;
import com.solactive.index.entity.IndexEntity;
import com.solactive.index.entity.ShareEntity;
import com.solactive.index.service.operation.DividendHandler;
import com.solactive.index.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DividendHandlerTest {

    private final DividendHandler dividendHandler = new DividendHandler();

    private IndexEntity indexEntity;

    @BeforeEach
    public void setUp() {
        List<ShareEntity> shares = new ArrayList<>();
        shares.add(new ShareEntity("A.OQ", 10.0, 20.0, 0, 0));
        shares.add(new ShareEntity("B.OQ", 20.0, 30.0, 0, 0));
        indexEntity = new IndexEntity("INDEX_1", shares, 1000.0);
    }

    @Test
    public void testExecuteSuccessfulDividendApplication() {
        DividendOperation dividendOperation = TestUtil.dividendOperation("A.OQ", 2.0);

        IndexEntity result = dividendHandler.execute(indexEntity, dividendOperation);

        assertNotNull(result);
        assertEquals(2, result.getIndexShares().size());
        ShareEntity updatedShare = result.getIndexShares().stream().filter(s -> s.getShareName().equals("A.OQ")).findFirst().orElse(null);

        assertNotNull(updatedShare);
        assertEquals(8.0, updatedShare.getSharePrice());
    }


    @Test
    public void testType() {
        assertEquals(OperationType.DIVIDEND, dividendHandler.type());
    }
}
