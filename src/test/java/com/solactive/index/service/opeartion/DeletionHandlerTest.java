package com.solactive.index.service.opeartion;


import com.solactive.index.dto.DeletionOperation;
import com.solactive.index.dto.OperationType;
import com.solactive.index.entity.IndexEntity;
import com.solactive.index.entity.ShareEntity;
import com.solactive.index.exception.IndexException;
import com.solactive.index.service.operation.DeletionHandler;
import com.solactive.index.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class DeletionHandlerTest {

    private final DeletionHandler deletionHandler = new DeletionHandler();

    private IndexEntity indexEntity;

    @BeforeEach
    public void setUp() {

        List<ShareEntity> shares = new ArrayList<>();
        shares.add(new ShareEntity("A.OQ", 10.0, 20.0, 0, 0));
        shares.add(new ShareEntity("B.OQ", 20.0, 30.0, 0, 0));
        shares.add(new ShareEntity("C.OQ", 20.0, 30.0, 0, 0));
        indexEntity = new IndexEntity("INDEX_1", shares, 1000.0);
    }

    @Test
    public void testExecuteSuccessfulDeletion() {
        DeletionOperation deletionOperation = TestUtil.deletionOperation("INDEX_1", "A.OQ");

        IndexEntity result = deletionHandler.execute(indexEntity, deletionOperation);

        assertNotNull(result);
        assertEquals(2, result.getIndexShares().size());
        assertTrue(result.getIndexShares().stream().noneMatch(s -> s.getShareName().equals("A.OQ")));
    }

    @Test
    public void testExecuteShareNotFound() {
        DeletionOperation deletionOperation = TestUtil.deletionOperation("INDEX_1", "D.OQ");

        IndexException exception = assertThrows(IndexException.class, () -> {
            deletionHandler.execute(indexEntity, deletionOperation);
        });

        assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
        assertEquals("404 NOT_FOUND \"Share not found in index\"", exception.getMessage());
    }

    @Test
    public void testExecuteIndexLessThanTwoMembers() {
        List<ShareEntity> shares = new ArrayList<>();
        shares.add(new ShareEntity("A.OQ", 10.0, 20.0, 0, 0));
        indexEntity = new IndexEntity("INDEX_1", shares, 1000.0);

        DeletionOperation deletionOperation = TestUtil.deletionOperation("INDEX_1", "A.OQ");

        IndexException exception = assertThrows(IndexException.class, () -> {
            deletionHandler.execute(indexEntity, deletionOperation);
        });

        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, exception.getStatusCode());
        assertEquals("405 METHOD_NOT_ALLOWED \"Index has less than two members after deletion\"", exception.getMessage());
    }

    @Test
    public void testType() {
        assertEquals(OperationType.DELETION, deletionHandler.type());
    }
}
