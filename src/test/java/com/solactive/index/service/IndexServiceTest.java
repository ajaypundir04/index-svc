package com.solactive.index.service;

import com.solactive.index.dto.*;
import com.solactive.index.entity.IndexEntity;
import com.solactive.index.exception.IndexException;
import com.solactive.index.repository.IndexRepository;
import com.solactive.index.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class IndexServiceTest {

    private final IndexRepository indexRepository = Mockito.mock(IndexRepository.class);

    private final IndexProcessor indexProcessor = Mockito.mock(IndexProcessor.class);

    private final Mapper mapper = Mockito.mock(Mapper.class);


    private final IndexService indexService = new IndexService(indexRepository, indexProcessor, mapper);

    private final IndexCreationRequest indexCreationRequest = TestUtil.indexCreationRequest(TestUtil.index("INDEX_1", List.of(TestUtil.share("A.OQ", 10.0, 20.0), TestUtil.share("B.OQ", 20.0, 30.0))));
    private final IndexEntity indexEntity = TestUtil.createDummyIndexEntity();


    @Test
    public void testCreateIndex() {
        when(indexRepository.indexExists("INDEX_1")).thenReturn(false);
        when(mapper.toEntity(any(Index.class))).thenReturn(indexEntity);
        when(indexProcessor.updateIndexValues(any(IndexEntity.class))).thenReturn(indexEntity);

        indexService.createIndex(indexCreationRequest);

        verify(indexRepository, times(1)).saveIndex(any(IndexEntity.class));
    }

    @Test
    public void testCreateIndexThrowsException() {
        when(indexRepository.indexExists("INDEX_1")).thenReturn(true);

        IndexException thrown = assertThrows(IndexException.class, () -> {
            indexService.createIndex(indexCreationRequest);
        });

        assertEquals(HttpStatus.CONFLICT, thrown.getStatusCode());
        assertEquals("409 CONFLICT \"Index already exists\"", thrown.getMessage());
    }

    @Test
    public void testAdjustIndex_withCreated() {
        IndexAdjustmentRequest indexAdjustmentRequest = indexAdjustmentRequest = TestUtil.indexAdjustmentRequest(TestUtil.additionOperation("INDEX_1", "A.OQ", 10.0, 20.0), null, null);
        when(indexRepository.getIndex("INDEX_1")).thenReturn(Optional.of(indexEntity));
        when(indexProcessor.applyAdjustment(any(Operation.class), any(IndexEntity.class))).thenReturn(indexEntity);
        when(indexProcessor.updateIndexValues(any(IndexEntity.class))).thenReturn(indexEntity);

        ResponseEntity<Void> response = indexService.adjustIndex(indexAdjustmentRequest);

        verify(indexRepository, times(1)).saveIndex(any(IndexEntity.class));
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void testAdjustIndex() {
        when(indexRepository.getIndex("INDEX_1")).thenReturn(Optional.of(indexEntity));
        when(indexProcessor.applyAdjustment(any(Operation.class), any(IndexEntity.class))).thenReturn(indexEntity);
        when(indexProcessor.updateIndexValues(any(IndexEntity.class))).thenReturn(indexEntity);

        ResponseEntity<Void> response = indexService.adjustIndex(TestUtil.indexAdjustmentRequest(null, TestUtil.deletionOperation("INDEX_1", "C"), null));

        verify(indexRepository, times(1)).saveIndex(any(IndexEntity.class));
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testAdjustIndex_dividend_member_not_found() {
        when(indexRepository.getAllIndices()).thenReturn(List.of(indexEntity));


        IndexException thrown = assertThrows(IndexException.class, () -> indexService.adjustIndex(TestUtil.indexAdjustmentRequest(null, null, TestUtil.dividendOperation("X", 1.0))));

        assertEquals(HttpStatusCode.valueOf(401), thrown.getStatusCode());
        assertEquals("401 UNAUTHORIZED \"member is not found in any index\"", thrown.getMessage());

        verify(indexRepository, times(0)).saveIndex(any(IndexEntity.class));
        verify(indexProcessor, times(0)).applyAdjustment(any(Operation.class), any(IndexEntity.class));
        verify(indexProcessor, times(0)).updateIndexValues(any(IndexEntity.class));
    }

    @Test
    public void testGetIndexState() {
        when(indexRepository.getIndex("INDEX_1")).thenReturn(Optional.of(indexEntity));
        when(mapper.toDto(any(IndexEntity.class))).thenReturn(TestUtil.indexResponse());

        IndexResponse response = indexService.getIndexState("INDEX_1");

        assertEquals("INDEX_1", response.indexDetails().indexName());
        assertEquals(4000.0, response.indexDetails().indexValue());
    }

    @Test
    public void testGetIndexStateThrowsException() {
        when(indexRepository.getIndex("INDEX_1")).thenReturn(Optional.empty());

        IndexException thrown = assertThrows(IndexException.class, () -> indexService.getIndexState("INDEX_1"));

        assertEquals(HttpStatus.NOT_FOUND, thrown.getStatusCode());
        assertEquals("404 NOT_FOUND \"Index does not exist\"", thrown.getMessage());
    }

    @Test
    void testGetAllIndexStates() {
        when(indexRepository.getAllIndices()).thenReturn(Collections.singletonList(indexEntity));
        when(mapper.toDto(anyList())).thenReturn(Collections.singletonList(TestUtil.indexResponse()));

        List<IndexResponse> responses = indexService.getAllIndexStates();

        assertEquals(1, responses.size());
        assertEquals("INDEX_1", responses.get(0).indexDetails().indexName());
    }
}
