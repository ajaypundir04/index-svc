package com.solactive.index.service;

import com.solactive.index.dto.*;
import com.solactive.index.entity.IndexEntity;
import com.solactive.index.entity.ShareEntity;
import com.solactive.index.exception.IndexException;
import com.solactive.index.service.operation.AdditionHandler;
import com.solactive.index.service.operation.DeletionHandler;
import com.solactive.index.service.operation.Handler;
import com.solactive.index.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.hamcrest.Matchers.closeTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class IndexProcessorTest {

    private final Handler additionHandler = Mockito.mock(AdditionHandler.class);
    private final Handler deletionHandler = Mockito.mock(DeletionHandler.class);
    private final Handler dividendHandler = Mockito.mock(DeletionHandler.class);
    private final IndexEntity indexEntity = TestUtil.createDummyIndexEntity();
    private IndexProcessor indexProcessor;

    @BeforeEach
    public void setUp() {

        when(additionHandler.type()).thenReturn(OperationType.ADDITION);
        when(deletionHandler.type()).thenReturn(OperationType.DELETION);
        when(dividendHandler.type()).thenReturn(OperationType.DIVIDEND);
        indexProcessor = new IndexProcessor(List.of(additionHandler, deletionHandler, dividendHandler));
        indexProcessor.initHandlers();
    }

    @Test
    public void testUpdateIndexValues() {
        IndexEntity updatedIndex = indexProcessor.updateIndexValues(indexEntity);

        assertNotNull(updatedIndex);
        assertEquals("INDEX_1", updatedIndex.getIndexName());
        assertEquals(0, Double.compare(4000.0, updatedIndex.getIndexValue()));


        List<ShareEntity> shares = updatedIndex.getIndexShares();
        assertEquals(4, shares.size());
        assertEquals(0, Double.compare(36.8574199806014, shares.get(0).getNumberOfShares()));
        assertEquals(0, Double.compare(7.37148399612028, shares.get(0).getIndexWeightPct()));
        assertEquals(0, Double.compare(294.8593598448112, shares.get(0).getIndexValue()));

    }

    @Test
    public void testApplyAdjustmentAddition() throws IndexException {
        AdditionOperation additionOperation = TestUtil.additionOperation("INDEX_1", "A1.OQ", 10.0, 20.0);

        when(additionHandler.execute(any(IndexEntity.class), any(Operation.class))).thenReturn(indexEntity);

        IndexEntity result = indexProcessor.applyAdjustment(additionOperation, indexEntity);

        assertNotNull(result);
        verify(additionHandler, times(1)).execute(any(IndexEntity.class), any(Operation.class));
    }

    @Test
    public void testApplyAdjustmentDeletion() throws IndexException {
        DeletionOperation deletionOperation = TestUtil.deletionOperation("INDEX_1", "B.OQ");
        when(deletionHandler.execute(any(IndexEntity.class), any(Operation.class))).thenReturn(indexEntity);

        IndexEntity result = indexProcessor.applyAdjustment(deletionOperation, indexEntity);

        assertNotNull(result);
        verify(deletionHandler, times(1)).execute(any(IndexEntity.class), any(Operation.class));
    }

    @Test
    public void testApplyAdjustmentDividend() throws IndexException {
        DividendOperation dividendOperation = TestUtil.dividendOperation("A.OQ", 2.0);
        when(dividendHandler.execute(any(IndexEntity.class), any(Operation.class))).thenReturn(indexEntity);
        IndexEntity result = indexProcessor.applyAdjustment(dividendOperation, indexEntity);
        assertNotNull(result);
        verify(dividendHandler, times(1)).execute(any(IndexEntity.class), any(Operation.class));
    }
}
