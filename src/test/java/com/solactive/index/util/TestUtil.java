package com.solactive.index.util;

import com.solactive.index.dto.*;
import com.solactive.index.entity.IndexEntity;
import com.solactive.index.entity.ShareEntity;
import com.solactive.index.utils.IndexUtils;

import java.util.Arrays;
import java.util.List;

public class TestUtil {

    private TestUtil() {
    }

    public static Share share(String name, double price, double num) {
        Share share = new Share();
        share.setShareName(name);
        share.setNumberOfShares(num);
        share.setSharePrice(price);
        return share;
    }

    public static Index index(String name, List<Share> shareList) {
        Index index = new Index();
        index.setIndexName(name);
        index.setIndexShares(shareList);
        return index;
    }

    public static IndexCreationRequest indexCreationRequest(Index index) {
        IndexCreationRequest indexCreationRequest = new IndexCreationRequest();
        indexCreationRequest.setIndex(index);
        return indexCreationRequest;
    }

    public static AdditionOperation additionOperation(String indexName,
                                                      String shareName, double price, double num) {
        AdditionOperation operation = new AdditionOperation();
        operation.setIndexName(indexName);
        operation.setShareName(shareName);
        operation.setSharePrice(price);
        operation.setNumberOfShares(num);
        return operation;
    }

    public static DeletionOperation deletionOperation(String indexName, String shareName) {
        DeletionOperation operation = new DeletionOperation();
        operation.setOperationType(OperationType.DELETION.getType());
        operation.setIndexName(indexName);
        operation.setShareName(shareName);
        return operation;
    }

    public static DividendOperation dividendOperation(String shareName, double value) {
        DividendOperation operation = new DividendOperation();
        operation.setOperationType(OperationType.DIVIDEND.getType());
        operation.setDividendValue(value);
        operation.setShareName(shareName);
        return operation;
    }

    public static IndexAdjustmentRequest indexAdjustmentRequest(
            AdditionOperation additionOperation, DeletionOperation deletionOperation,
            DividendOperation dividendOperation
    ) {
        IndexAdjustmentRequest request = new IndexAdjustmentRequest();
        request.setAdditionOperation(additionOperation);
        request.setDividendOperation(dividendOperation);
        request.setDeletionOperation(deletionOperation);
        return request;
    }

    public static IndexResponse indexResponse() {
        return IndexUtils.indexResponse(createDummyIndexEntity());
    }

    public static IndexEntity createDummyIndexEntity() {
        List<ShareEntity> shares = Arrays.asList(
                new ShareEntity("A.OQ", 8.0, 36.8574199806014, 7.37148399612027, 294.859359844811),
                new ShareEntity("B.OQ", 20.0, 55.286129970902, 27.643064985451, 1105.72259941804),
                new ShareEntity("C.OQ", 30.0, 73.7148399612027, 55.286129970902, 2211.44519883608),
                new ShareEntity("E.OQ", 10.0, 38.7972841901067, 9.69932104752667, 387.972841901067)
        );
        return new IndexEntity("INDEX_1", shares, 4000.0);
    }
}
