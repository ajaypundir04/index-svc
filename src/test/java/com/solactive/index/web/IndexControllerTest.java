package com.solactive.index.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solactive.index.dto.IndexAdjustmentRequest;
import com.solactive.index.dto.IndexCreationRequest;
import com.solactive.index.dto.IndexResponse;
import com.solactive.index.service.IndexService;
import com.solactive.index.util.TestUtil;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(IndexController.class)
public class IndexControllerTest {

    private final ObjectMapper mapper = new ObjectMapper();
    private final IndexCreationRequest indexCreationRequest = TestUtil.indexCreationRequest(
            TestUtil.index("INDEX_1", List.of(
                    TestUtil.share("A", 1, 2),
                    TestUtil.share("B", 2, 3)
            ))
    );
    private final IndexAdjustmentRequest indexAdjustmentRequest = TestUtil.indexAdjustmentRequest(
            TestUtil.additionOperation("INDEX_1", "C", 1, 2),
            null, null);
    private final IndexResponse indexResponse = TestUtil.indexResponse();
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private IndexService indexService;

    @Test
    public void testCreateIndex() throws Exception {
        doNothing().when(indexService).createIndex(any(IndexCreationRequest.class));

        mockMvc.perform(MockMvcRequestBuilders.post("/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(indexCreationRequest)))
                .andExpect(status().isCreated());
    }

    @Test
    public void testCreateIndex_badRequest() throws Exception {
        doNothing().when(indexService).createIndex(any(IndexCreationRequest.class));

        mockMvc.perform(MockMvcRequestBuilders.post("/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"index\": { \"indexName\": \"INDEX_1\", \"indexShares\": [] } }"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAdjustIndex() throws Exception {
        when(indexService.adjustIndex(any(IndexAdjustmentRequest.class)))
                .thenReturn(new ResponseEntity<>(HttpStatusCode.valueOf(200)));

        mockMvc.perform(MockMvcRequestBuilders.post("/indexAdjustment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(indexAdjustmentRequest)))
                .andExpect(status().isOk());
    }

    @Test
    public void testAdjustIndex_badRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/indexAdjustment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
        mockMvc.perform(MockMvcRequestBuilders.post("/indexAdjustment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(TestUtil.indexAdjustmentRequest(
                                TestUtil.additionOperation("INDEX_1", "C", 1, 2),
                                TestUtil.deletionOperation("INDEX_1", "A"),
                                null
                        ))))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetAllIndexStates() throws Exception {
        when(indexService.getAllIndexStates()).thenReturn(List.of(indexResponse));

        mockMvc.perform(MockMvcRequestBuilders.get("/indexState"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetIndexState() throws Exception {
        when(indexService.getIndexState("INDEX_1")).thenReturn(indexResponse);

        mockMvc.perform(MockMvcRequestBuilders.get("/indexState/INDEX_1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.indexDetails.indexName", Matchers.is("INDEX_1")));
    }

    @Test
    public void testGetIndexStateBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/indexState/"))
                .andExpect(status().is4xxClientError());
    }
}
