package com.solactive.index.intergration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solactive.index.dto.*;
import com.solactive.index.repository.IndexRepository;
import com.solactive.index.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class IndexControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private IndexRepository indexRepository;

    private IndexCreationRequest indexCreationRequest;
    private AdditionOperation additionOperation;
    private DeletionOperation deletionOperation;
    private DividendOperation dividendOperation;

    @BeforeEach
    void setUp() {
        indexRepository.getAllIndices().forEach(index -> indexRepository.saveIndex(null)); // Clear the repository before each test

        Share shareA = TestUtil.share("A.OQ", 10.0, 20.0);
        Share shareB = TestUtil.share("B.OQ", 20.0, 30.0);
        Share shareC = TestUtil.share("C.OQ", 30.0, 40.0);
        Share shareD = TestUtil.share("D.OQ", 40.0, 50.0);

        Index index = TestUtil.index("INDEX_1", List.of(shareA, shareB, shareC, shareD));
        indexCreationRequest = TestUtil.indexCreationRequest(index);

        additionOperation = TestUtil.additionOperation("INDEX_1", "E.OQ", 10.0, 20.0);

        deletionOperation = TestUtil.deletionOperation("INDEX_1", "D.OQ");
        dividendOperation = TestUtil.dividendOperation("A.OQ", 2.0);
    }


    @Test
    void testFullIntegration() throws Exception {
        // Step 1: Create Index
        mockMvc.perform(post("/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(indexCreationRequest)))
                .andExpect(status().isCreated());

        // Verify the state after creation
        mockMvc.perform(get("/indexState/INDEX_1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.indexDetails.indexName", is("INDEX_1")))
                .andExpect(jsonPath("$.indexDetails.indexMembers[0].shareName", is("A.OQ")))
                .andExpect(jsonPath("$.indexDetails.indexMembers[1].shareName", is("B.OQ")))
                .andExpect(jsonPath("$.indexDetails.indexMembers[2].shareName", is("C.OQ")))
                .andExpect(jsonPath("$.indexDetails.indexMembers[3].shareName", is("D.OQ")));

        // Step 2: Add Share
        IndexAdjustmentRequest additionRequest = TestUtil.indexAdjustmentRequest(additionOperation, null, null);
        mockMvc.perform(post("/indexAdjustment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(additionRequest)))
                .andExpect(status().isCreated());

        // Verify the state after addition
        mockMvc.perform(get("/indexState/INDEX_1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.indexDetails.indexMembers[4].shareName", is("E.OQ")));

        // Step 3: Delete Share
        IndexAdjustmentRequest deletionRequest = TestUtil.indexAdjustmentRequest(null, deletionOperation, null);
        mockMvc.perform(post("/indexAdjustment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(deletionRequest)))
                .andExpect(status().isOk());

        // Verify the state after deletion
        mockMvc.perform(get("/indexState/INDEX_1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.indexDetails.indexMembers[4].shareName").doesNotExist());

        // Step 4: Apply Dividend
        IndexAdjustmentRequest dividendRequest = TestUtil.indexAdjustmentRequest(null, null, dividendOperation);
        mockMvc.perform(post("/indexAdjustment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(dividendRequest)))
                .andExpect(status().isOk());

        // Verify the state after applying the dividend
        System.out.println(mockMvc.perform(get("/indexState/INDEX_1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.indexDetails.indexMembers[0].shareName", is("A.OQ")))
                .andExpect(jsonPath("$.indexDetails.indexMembers[0].sharePrice", is(8.0)))
                .andReturn().getResponse().getContentAsString());
    }

}
