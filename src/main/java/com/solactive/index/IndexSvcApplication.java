package com.solactive.index;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndexSvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(IndexSvcApplication.class, args);
	}

}
