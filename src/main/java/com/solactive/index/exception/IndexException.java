package com.solactive.index.exception;

import org.springframework.http.HttpStatusCode;
import org.springframework.web.server.ResponseStatusException;

public class IndexException extends ResponseStatusException {

    public IndexException(HttpStatusCode status, String reason) {
        super(status, reason);
    }

}
