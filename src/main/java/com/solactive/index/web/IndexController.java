package com.solactive.index.web;

import com.solactive.index.dto.IndexAdjustmentRequest;
import com.solactive.index.dto.IndexCreationRequest;
import com.solactive.index.dto.IndexResponse;
import com.solactive.index.service.IndexService;
import com.solactive.index.utils.IndexUtils;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@Validated
public class IndexController {

    private final IndexService indexService;

    @Autowired
    public IndexController(IndexService indexService) {
        this.indexService = indexService;
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public void createIndex(@Valid @RequestBody IndexCreationRequest indexCreationRequest) {
        this.indexService.createIndex(indexCreationRequest);
    }

    @PostMapping("/indexAdjustment")
    public ResponseEntity<Void> adjustIndex(@Valid @RequestBody IndexAdjustmentRequest indexCreationRequest) {
        return this.indexService.adjustIndex(indexCreationRequest);
    }

    @GetMapping("/indexState")
    @ResponseStatus(HttpStatus.OK)
    public List<IndexResponse> getAllIndexStates() {
        return this.indexService.getAllIndexStates();
    }

    @GetMapping("/indexState/{index_name}")
    @ResponseStatus(HttpStatus.OK)
    public IndexResponse getIndexState(@PathVariable("index_name")
                                       @NotBlank(message = IndexUtils.INDEX_NAME_ERROR_MSG) String indexName) {
        return this.indexService.getIndexState(indexName);
    }
}
