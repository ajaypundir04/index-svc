package com.solactive.index.validator;


import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {IndexAdjustmentRequestValidator.class})
@Documented
public @interface ValidIndexAdjustmentRequest {

    String message() default "Invalid Index Adjustment Request";

    Class<?>[] groups() default {};

    Class<?extends Payload> [] payload() default {};

}
