package com.solactive.index.validator;

import com.solactive.index.dto.IndexAdjustmentRequest;

import java.util.Objects;

public interface IndexAdjustmentRequestRules {
    String validate(IndexAdjustmentRequest value);

    enum Impl implements IndexAdjustmentRequestRules {
        ADDITION {
            @Override
            public String validate(IndexAdjustmentRequest value) {
                return Objects.nonNull(value.getAdditionOperation()) ?
                        "It is a Addition Operation" : null;

            }

        },
        DELETION {
            @Override
            public String validate(IndexAdjustmentRequest value) {
                return Objects.nonNull(value.getDeletionOperation()) ?
                        "It is a Deletion Operation" : null;
            }
        },
        DIVIDEND {
            @Override
            public String validate(IndexAdjustmentRequest value) {
                return Objects.nonNull(value.getDividendOperation()) ?
                        "It is a Dividend Operation" : null;
            }
        }
    }
}

