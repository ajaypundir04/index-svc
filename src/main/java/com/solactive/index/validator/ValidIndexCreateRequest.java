package com.solactive.index.validator;


import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

@Target({ElementType.TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {IndexCreateRequestValidator.class})
@Documented
public @interface ValidIndexCreateRequest {

    String message() default "Invalid Index Request";

    Class<?>[] groups() default {};

    Class<?extends Payload> [] payload() default {};

}
