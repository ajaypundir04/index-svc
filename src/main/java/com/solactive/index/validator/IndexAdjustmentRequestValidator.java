package com.solactive.index.validator;

import com.solactive.index.dto.IndexAdjustmentRequest;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class IndexAdjustmentRequestValidator implements ConstraintValidator<ValidIndexAdjustmentRequest, IndexAdjustmentRequest> {


    @Override
    public boolean isValid(IndexAdjustmentRequest indexAdjustmentRequest,
                           ConstraintValidatorContext constraintValidatorContext) {
        constraintValidatorContext.disableDefaultConstraintViolation();
        List<String> message = new ArrayList<>();
        for (IndexAdjustmentRequestRules validator : IndexAdjustmentRequestRules.Impl.values()) {
            message.add(validator.validate(indexAdjustmentRequest));
        }
        message.forEach(e -> {
            if (Objects.nonNull(e)) constraintValidatorContext
                    .buildConstraintViolationWithTemplate(e).addConstraintViolation();
        });
        int nonNullCount = message.stream().filter(Objects::nonNull).toList().size();
        if (nonNullCount == 0) {
            constraintValidatorContext
                    .buildConstraintViolationWithTemplate("Empty Operation").addConstraintViolation();
        }

        return nonNullCount == 1;
    }


}
