package com.solactive.index.validator;

import com.solactive.index.dto.IndexCreationRequest;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class IndexCreateRequestValidator implements ConstraintValidator<ValidIndexCreateRequest, IndexCreationRequest> {
    @Override
    public boolean isValid(IndexCreationRequest indexCreationRequest,
                           ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();

        List<String> errorMessages = Arrays.stream(IndexCreateRequestRules.Impl.values())
                .map(validator -> validator.validate(indexCreationRequest))
                .filter(Objects::nonNull)
                .toList();

        errorMessages.forEach(errorMessage -> context.buildConstraintViolationWithTemplate(errorMessage)
                .addConstraintViolation());

        return errorMessages.isEmpty();
    }

}
