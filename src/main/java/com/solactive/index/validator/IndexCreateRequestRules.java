package com.solactive.index.validator;

import com.solactive.index.dto.IndexCreationRequest;

public interface IndexCreateRequestRules {
    String validate(IndexCreationRequest value);

    enum Impl implements IndexCreateRequestRules {
        NUMBER_OF_SHARES {
            @Override
            public String validate(IndexCreationRequest value) {

                if (null == value.getIndex().getIndexShares()
                        || value.getIndex().getIndexShares().size() < 2) {
                    return "Each index should have at least two members";
                }
                return null;
            }
        }
    }
}

