package com.solactive.index.repository;

import com.solactive.index.entity.IndexEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class IndexRepository {
    private final Map<String, IndexEntity> indexStore = new ConcurrentHashMap<>();

    public boolean indexExists(String indexName) {
        return indexStore.containsKey(indexName);
    }

    public void saveIndex(IndexEntity index) {
        indexStore.put(index.getIndexName(), index);
    }

    public Optional<IndexEntity> getIndex(String indexName) {
        return Optional.ofNullable(indexStore.get(indexName));
    }

    public List<IndexEntity> getAllIndices() {
        return new ArrayList<>(indexStore.values());
    }
}
