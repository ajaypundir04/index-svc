package com.solactive.index.service;

import com.solactive.index.dto.Index;
import com.solactive.index.dto.IndexResponse;
import com.solactive.index.dto.Share;
import com.solactive.index.entity.IndexEntity;
import com.solactive.index.entity.ShareEntity;
import com.solactive.index.utils.IndexUtils;
import org.springframework.stereotype.Component;

import java.util.List;

public interface Mapper {
    IndexEntity toEntity(Index index);

    IndexResponse toDto(IndexEntity indexEntity);

    List<IndexResponse> toDto(List<IndexEntity> allIndices);

    @Component
    class Impl implements Mapper {

        @Override
        public IndexEntity toEntity(Index index) {
            return new IndexEntity(index.getIndexName(),
                    index.getIndexShares().stream().map(this::toEntity).toList(), 0.0);
        }

        private ShareEntity toEntity(Share share) {
            return IndexUtils.shareEntity(share);
        }



        @Override
        public IndexResponse toDto(IndexEntity indexEntity) {
            return
                    IndexUtils.indexResponse(indexEntity);
        }



        @Override
        public List<IndexResponse> toDto(List<IndexEntity> allIndices) {
            return allIndices.stream().map(this::toDto).toList();
        }


    }
}
