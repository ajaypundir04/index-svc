package com.solactive.index.service;

import com.solactive.index.dto.Operation;
import com.solactive.index.dto.OperationType;
import com.solactive.index.entity.IndexEntity;
import com.solactive.index.entity.ShareEntity;
import com.solactive.index.exception.IndexException;
import com.solactive.index.service.operation.Handler;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class IndexProcessor {

    private final List<Handler> handlers;
    private final Map<OperationType, Handler> executorMap = new HashMap<>();

    @Autowired
    public IndexProcessor(List<Handler> handlers) {
        this.handlers = handlers;
    }

    @PostConstruct
    public void initHandlers() {
        handlers.forEach(executor -> executorMap.put(executor.type(), executor));
    }

    public IndexEntity updateIndexValues(IndexEntity index) {
        double totalMarketValue = calculateTotalMarketValue(index);
        List<ShareEntity> updatedShares = calculateUpdatedShares(index, totalMarketValue);
        double totalIndexValue = calculateTotalIndexValue(updatedShares);

        return new IndexEntity(index.getIndexName(), updatedShares, Math.ceil(totalIndexValue));
    }

    private double calculateTotalMarketValue(IndexEntity index) {
        return index.getIndexShares().stream()
                .mapToDouble(share -> share.getSharePrice() * share.getNumberOfShares())
                .sum();
    }

    private List<ShareEntity> calculateUpdatedShares(IndexEntity index, double totalMarketValue) {
        List<ShareEntity> updatedShares = new ArrayList<>();

        for (ShareEntity share : index.getIndexShares()) {
            ShareEntity updatedShare = performShareCalculations(share, totalMarketValue);
            updatedShares.add(updatedShare);
        }

        return updatedShares;
    }

    private ShareEntity performShareCalculations(ShareEntity share, double totalMarketValue) {
        double marketValue = share.getSharePrice() * share.getNumberOfShares();
        double idxWeight = (marketValue / totalMarketValue) * 100;
        double adjustedNumberOfShares = (idxWeight / 100) * (totalMarketValue / share.getSharePrice());
        double adjustedMarketValue = share.getSharePrice() * adjustedNumberOfShares;

        return new ShareEntity(share.getShareName(), share.getSharePrice(),
                adjustedNumberOfShares, idxWeight, adjustedMarketValue);
    }

    private double calculateTotalIndexValue(List<ShareEntity> updatedShares) {
        return updatedShares.stream()
                .mapToDouble(ShareEntity::getIndexValue)
                .sum();
    }


    public IndexEntity applyAdjustment(Operation operation, IndexEntity index) throws IndexException {
        return executorMap.getOrDefault(operation.type(), executorMap.get(OperationType.UN_SUPPORTED)).execute(index, operation);
    }


}
