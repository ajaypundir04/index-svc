package com.solactive.index.service.operation;

import com.solactive.index.dto.Operation;
import com.solactive.index.dto.OperationType;
import com.solactive.index.entity.IndexEntity;
import com.solactive.index.exception.IndexException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class UnSupportedHandler implements Handler {
    @Override
    public IndexEntity execute(IndexEntity indexEntity, Operation operation) {
        throw new IndexException(HttpStatus.BAD_REQUEST, "Unsupported Operation");

    }

    @Override
    public OperationType type() {
        return OperationType.UN_SUPPORTED;
    }
}
