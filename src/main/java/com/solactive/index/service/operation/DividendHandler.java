package com.solactive.index.service.operation;

import com.solactive.index.dto.DividendOperation;
import com.solactive.index.dto.Operation;
import com.solactive.index.dto.OperationType;
import com.solactive.index.entity.IndexEntity;
import org.springframework.stereotype.Component;

@Component
public class DividendHandler implements Handler {
    @Override
    public IndexEntity execute(IndexEntity indexEntity,
                               Operation operation) {
        DividendOperation op = (DividendOperation) operation;

        indexEntity.setIndexShares(indexEntity.getIndexShares()
                .stream()
                .peek(s -> {
                            if (s.getShareName().equals(op.getShareName())) {
                                s.setSharePrice(s.getSharePrice() - op.getDividendValue());
                            }
                        }
                ).toList());

        return indexEntity;
    }

    @Override
    public OperationType type() {
        return OperationType.DIVIDEND;
    }
}
