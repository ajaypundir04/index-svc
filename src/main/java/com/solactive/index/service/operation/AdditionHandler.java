package com.solactive.index.service.operation;

import com.solactive.index.dto.AdditionOperation;
import com.solactive.index.dto.Operation;
import com.solactive.index.dto.OperationType;
import com.solactive.index.dto.Share;
import com.solactive.index.entity.IndexEntity;
import com.solactive.index.entity.ShareEntity;
import com.solactive.index.exception.IndexException;
import com.solactive.index.utils.IndexUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class AdditionHandler implements Handler {
    @Override
    public IndexEntity execute(IndexEntity indexEntity,
                               Operation operation) {
        AdditionOperation op = (AdditionOperation) operation;
        ShareEntity shareEntity = IndexUtils.shareEntity(toShare(op));
        if (indexEntity.getIndexShares().stream().anyMatch(s -> s.getShareName().equals(shareEntity.getShareName()))) {
            throw new IndexException(HttpStatus.ACCEPTED, "Share Already Exists");
        }
        indexEntity.getIndexShares()
                .add(shareEntity);
        return indexEntity;
    }

    @Override
    public OperationType type() {
        return OperationType.ADDITION;
    }

    private Share toShare(AdditionOperation operation) {
        Share share = new Share();
        share.setShareName(operation.getShareName());
        share.setSharePrice(operation.getSharePrice());
        share.setNumberOfShares(operation.getNumberOfShares());
        return share;
    }
}
