package com.solactive.index.service.operation;

import com.solactive.index.dto.Operation;
import com.solactive.index.dto.OperationType;
import com.solactive.index.entity.IndexEntity;

public interface Handler {
    IndexEntity execute(IndexEntity indexEntity,
                        Operation operation);

    OperationType type();
}
