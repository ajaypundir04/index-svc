package com.solactive.index.service.operation;

import com.solactive.index.dto.DeletionOperation;
import com.solactive.index.dto.Operation;
import com.solactive.index.dto.OperationType;
import com.solactive.index.entity.IndexEntity;
import com.solactive.index.exception.IndexException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class DeletionHandler implements Handler {
    @Override
    public IndexEntity execute(IndexEntity indexEntity,
                               Operation operation) {
        DeletionOperation op = (DeletionOperation) operation;

        if (indexEntity.getIndexShares().stream().noneMatch(s -> s.getShareName().equals(op.getShareName()))) {
            throw new IndexException(HttpStatus.NOT_FOUND, "Share not found in index");
        }
        if (indexEntity.getIndexShares().size() - 1 < 2) {
            throw new IndexException(HttpStatus.METHOD_NOT_ALLOWED, "Index has less than two members after deletion");
        }
        indexEntity.getIndexShares().removeIf(s -> op.getShareName().equalsIgnoreCase(s.getShareName()));
        return indexEntity;
    }

    @Override
    public OperationType type() {
        return OperationType.DELETION;
    }
}
