package com.solactive.index.service;

import com.solactive.index.dto.*;
import com.solactive.index.entity.IndexEntity;
import com.solactive.index.exception.IndexException;
import com.solactive.index.repository.IndexRepository;
import com.solactive.index.utils.IndexUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class IndexService {
    private final IndexRepository indexRepository;
    private final IndexProcessor indexProcessor;
    private final Mapper mapper;

    @Autowired
    public IndexService(IndexRepository indexRepository, IndexProcessor indexProcessor, Mapper mapper) {
        this.indexRepository = indexRepository;
        this.indexProcessor = indexProcessor;
        this.mapper = mapper;
    }

    public void createIndex(IndexCreationRequest request) {
        if (indexRepository.indexExists(request.getIndex().getIndexName())) {
            throw new IndexException(HttpStatus.CONFLICT, "Index already exists");
        }
        IndexEntity indexEntity = this.mapper.toEntity(request.getIndex());
        updateAndSaveIndex(indexEntity);
    }

    private void updateAndSaveIndex(IndexEntity indexEntity) {
        IndexEntity index = indexProcessor.updateIndexValues(indexEntity);
        indexRepository.saveIndex(index);
    }

    public ResponseEntity<Void> adjustIndex(IndexAdjustmentRequest request) {
        Operation operation = IndexUtils.getOperationFromRequest(request);
        if (operation.type() == OperationType.DIVIDEND) {
            DividendOperation dividendOperation = (DividendOperation) operation;
            matchingIndex(dividendOperation.getShareName()).forEach(indexEntity -> {
               performOperation(operation, indexEntity);
            });
        } else {
            performOperation(operation, getIndex(operation.indexName()));
        }
        HttpStatus code = operation.type() == OperationType.ADDITION ? HttpStatus.CREATED : HttpStatus.OK;
        return new ResponseEntity<>(code);

    }

    private void performOperation(Operation operation, IndexEntity indexEntity) {
        IndexEntity index = indexProcessor.applyAdjustment(operation, indexEntity);
        updateAndSaveIndex(index);
    }

    private IndexEntity getIndex(String indexName) {
        return indexRepository.getIndex(indexName).orElseThrow(() -> new IndexException(HttpStatus.NOT_FOUND, "Index does not exist"));
    }

    private List<IndexEntity> matchingIndex(String shareName) {
        List<IndexEntity> matchedIndex = new ArrayList<>();
        for (IndexEntity indexEntity : indexRepository.getAllIndices()) {
            if (indexEntity.getIndexShares().stream().anyMatch(s -> s.getShareName().equalsIgnoreCase(shareName))) {
                matchedIndex.add(indexEntity);
            }
        }
        if (matchedIndex.isEmpty()) {
            throw new IndexException(HttpStatusCode.valueOf(401), "member is not found in any index");
        }
        return matchedIndex;
    }

    public IndexResponse getIndexState(String indexName) {
        IndexEntity indexEntity = getIndex(indexName);
        return mapper.toDto(indexEntity);
    }

    public List<IndexResponse> getAllIndexStates() {
        return mapper.toDto(indexRepository.getAllIndices());
    }
}
