package com.solactive.index.dto;

public interface Operation {
    OperationType type();
    String indexName();
}
