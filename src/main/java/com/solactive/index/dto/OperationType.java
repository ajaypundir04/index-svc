package com.solactive.index.dto;

import java.util.Arrays;

public enum OperationType {

    ADDITION("ADDITION"), DELETION("DELETION"), DIVIDEND("CASH_DIVIDEND"), UN_SUPPORTED("UN_KNOWN");
    private final String type;

    OperationType(String type) {
        this.type = type;
    }

    public static OperationType getOperationType(String type) {
        return Arrays.stream(OperationType.values())
                .filter(s -> type.equalsIgnoreCase(s.type))
                .findFirst()
                .orElse(OperationType.UN_SUPPORTED);
    }

    public String getType() {
        return type;
    }

}
