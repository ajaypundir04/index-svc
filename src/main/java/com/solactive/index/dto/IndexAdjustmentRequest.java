package com.solactive.index.dto;

import com.solactive.index.validator.ValidIndexAdjustmentRequest;
import jakarta.validation.Valid;
import lombok.Data;

@Data
@ValidIndexAdjustmentRequest
public class IndexAdjustmentRequest {
    @Valid
    private AdditionOperation additionOperation;
    @Valid
    private DeletionOperation deletionOperation;
    @Valid
    private DividendOperation dividendOperation;

}