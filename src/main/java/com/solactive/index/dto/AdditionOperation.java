package com.solactive.index.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.solactive.index.utils.IndexUtils;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
@JsonTypeName("additionOperation")
public final class AdditionOperation extends Share implements Operation {
    @NotBlank(message = IndexUtils.INDEX_NAME_ERROR_MSG)
    private String indexName;

    private String operationType = "ADDITION";

    @Override
    public OperationType type() {
        return OperationType.getOperationType(this.operationType);
    }

    @Override
    public String indexName() {
        return this.indexName;
    }
}
