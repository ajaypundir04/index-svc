package com.solactive.index.dto;

import java.util.List;

public record IndexResponse(IndexDetails indexDetails) {
    public record IndexMember(
            String shareName,
            double sharePrice,
            double numberOfshares,
            double indexWeightPct,
            double indexValue
    ) {
    }

    public record IndexDetails(
            String indexName,
            double indexValue,
            List<IndexResponse.IndexMember> indexMembers) {
    }
}
