package com.solactive.index.dto;

import com.solactive.index.validator.ValidIndexCreateRequest;
import jakarta.validation.Valid;
import lombok.Data;


@ValidIndexCreateRequest
@Data
public class IndexCreationRequest {
    @Valid
    private Index index;
}