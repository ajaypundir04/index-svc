package com.solactive.index.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.solactive.index.utils.IndexUtils;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
public class Share {
    @NotBlank(message = IndexUtils.SHARE_NAME_ERROR_MSG)
    private String shareName;
    @Positive(message = IndexUtils.SHARE_PRICE_ERROR_MSG)
    private double sharePrice;
    @JsonProperty("numberOfshares")
    @Positive(message = IndexUtils.NUM_SHARE_PRICE_ERROR_MSG)
    private double numberOfShares;
}