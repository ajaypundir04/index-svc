package com.solactive.index.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.solactive.index.utils.IndexUtils;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
@JsonTypeName("deletionOperation")
public final class DeletionOperation implements Operation{
    @NotBlank(message = IndexUtils.INDEX_NAME_ERROR_MSG)
    private String indexName;

    @NotBlank(message = IndexUtils.SHARE_NAME_ERROR_MSG)
    private String shareName;

    @NotBlank(message = IndexUtils.OPERATION_TYPE_ERROR_MSG)
    private String operationType;

    @Override
    public OperationType type() {
        return OperationType.getOperationType(this.operationType);
    }

    @Override
    public String indexName() {
        return this.indexName;
    }
}
