package com.solactive.index.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.solactive.index.utils.IndexUtils;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.List;

@Data
public class Index {
    @NotBlank(message = IndexUtils.INDEX_NAME_ERROR_MSG)
    private String indexName;
    @Valid
    @JsonProperty("indexshares")
    private List<Share> indexShares;
}