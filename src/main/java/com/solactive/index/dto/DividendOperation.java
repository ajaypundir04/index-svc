package com.solactive.index.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.solactive.index.utils.IndexUtils;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
@JsonTypeName("dividendOperation")
public final class DividendOperation implements Operation {
    @NotBlank(message = IndexUtils.SHARE_NAME_ERROR_MSG)
    private String shareName;

    @Positive(message = IndexUtils.DIVIDEND_ERROR_MSG)
    private double dividendValue;

    @NotBlank(message = IndexUtils.OPERATION_TYPE_ERROR_MSG)
    private String operationType;

    @Override
    public OperationType type() {
        return OperationType.getOperationType(this.operationType);
    }

    @Override
    public String indexName() {
        return IndexUtils.ALL_INDICIES;
    }
}
