package com.solactive.index.utils;

import com.solactive.index.dto.IndexAdjustmentRequest;
import com.solactive.index.dto.IndexResponse;
import com.solactive.index.dto.Operation;
import com.solactive.index.dto.Share;
import com.solactive.index.entity.IndexEntity;
import com.solactive.index.entity.ShareEntity;

import java.util.Optional;
import java.util.stream.Stream;

public class IndexUtils {
    public static final String SHARE_PRICE_ERROR_MSG = "Share price is always positive";
    public static final String DIVIDEND_ERROR_MSG = "Dividend is always positive";
    public static final String SHARE_NAME_ERROR_MSG = "Share name cannot be blank or null";
    public static final String INDEX_NAME_ERROR_MSG = "Index name cannot be blank or null";
    public static final String NUM_SHARE_PRICE_ERROR_MSG = "'Number of shares' for a share " +
            "in an index should always be positive";

    public static final String OPERATION_TYPE_ERROR_MSG = "Operation Type can not be blank";
    public static final String ALL_INDICIES = "ALL";


    private IndexUtils() {
    }

    public static Operation getOperationFromRequest(IndexAdjustmentRequest request) {
        return Stream.of(
                        Optional.ofNullable(request.getAdditionOperation()),
                        Optional.ofNullable(request.getDeletionOperation()),
                        Optional.ofNullable(request.getDividendOperation())
                )
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst()
                .orElse(null);
    }

    public static ShareEntity shareEntity(Share share) {
        return new ShareEntity(share.getShareName(), share.getSharePrice(),
                share.getNumberOfShares(), 0.0,
                share.getSharePrice() * share.getNumberOfShares());
    }

    public static IndexResponse indexResponse(IndexEntity indexEntity) {
        return new IndexResponse(new IndexResponse.IndexDetails(
                indexEntity.getIndexName(),
                indexEntity.getIndexValue(),
                indexEntity.getIndexShares()
                        .stream()
                        .map(s -> new IndexResponse.IndexMember(
                                s.getShareName(), s.getSharePrice(),
                                s.getNumberOfShares(), s.getIndexWeightPct(),
                                s.getIndexValue()
                        )).toList()
        ));
    }
}
