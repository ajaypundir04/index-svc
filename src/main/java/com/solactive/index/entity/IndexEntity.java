package com.solactive.index.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class IndexEntity {
    private String indexName;
    private List<ShareEntity> indexShares;
    private double indexValue;

}
