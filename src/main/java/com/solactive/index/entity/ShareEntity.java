package com.solactive.index.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ShareEntity {
    private String shareName;
    private double sharePrice;
    private double numberOfShares;
    private double indexWeightPct;
    private double indexValue;


}
