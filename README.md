# Index Service (index-svc)

## Overview

The `index-svc` is a Spring Boot-based microservice designed to manage financial indices, including the creation,
adjustment, and retrieval of index states. This service supports operations such as addition, deletion, and dividend
application on the shares within an index.

## Design Explanation

### Components

1. **DTO (Data Transfer Objects)**: These are used to transfer data between processes. Key DTOs include:
    - `IndexCreationRequest`
    - `IndexAdjustmentRequest`
    - `IndexResponse`
    - `AdditionOperation`
    - `DeletionOperation`
    - `DividendOperation`

2. **Entities**: Represent the core data structures.
    - `IndexEntity`
    - `ShareEntity`

3. **Service Layer**: Contains the business logic.
    - `IndexService`: Manages the creation, adjustment, and retrieval of indices.
    - `IndexProcessor`: Processes the adjustments on indices using various handlers.

4. **Operation Handlers**: Implement specific operations on the index.
    - `AdditionHandler`
    - `DeletionHandler`
    - `DividendHandler`

5. **Utils**: Utility classes for common tasks.
    - `IndexUtils`: Provides utility methods for the application.

6. **Controller**: Exposes REST endpoints for interacting with the service.
    - `IndexController`

### Flows

1. **Index Creation**:
    - Request: `POST /create`
    - Flow: `IndexController -> IndexService -> IndexProcessor -> IndexRepository`
    - Validates if the index already exists, then creates and saves a new index.

2. **Index Adjustment**:
    - Request: `POST /indexAdjustment`
    - Flow: `IndexController -> IndexService -> IndexProcessor -> Handlers -> IndexRepository`
    - Depending on the type of operation (Addition, Deletion, Dividend), the corresponding handler processes the
      request.

3. **Retrieve Index State**:
    - Request: 
      - `GET /indexState/{index_name}` specific index
      - `GET /indexState/` get all index
    - Flow: `IndexController -> IndexService -> IndexRepository`
    - Retrieves and returns the state of the specified index.

### Assumptions Made

1. The index operations (addition, deletion, dividend) are independent and do not need to be atomic across multiple
   indices. Also we can have only one opearion per adjustmentTime.
2. The system does not require real-time updates to be visible immediately across all clients.
3. The service runs in a controlled environment where data consistency is managed.    

### Future Changes and Considerations

1. **Distributed Environment**:
    - Implement a distributed cache (e.g., Redis) or a distributed database (e.g., Cassandra) for the `IndexRepository`.
    - Use service discovery and load balancing to manage multiple instances of the service.

2. **Scalability**:
    - Introduce asynchronous processing for long-running operations.
        - /indexAdjustment API can be non-blocking to process the index calculations.
    - Implement proper logging and monitoring (e.g., ELK stack).

3. **Security**:
    - Implement authentication and authorization.
    - Use HTTPS for secure communication.

4. **Consistency**:
    - Implement consistency for index calculation. 
    - Implement concurrency for multiple operation.          

### Personal thoughts

I enjoyed the challenge as it provided an opportunity to design a system from scratch, consider different architectural
choices, and implement a service with the required functionality. The Index calculation is quite interesting and
challenging part of the whole assignment.

## Running the Project

### Prerequisites

- Java 22
- Maven
- Docker (for containerization)

### Run-Setup

1. **Clone the repository**:
   ```sh
   git clone https://gitlab.com/ajaypundir04/index-svc
   cd index-svc
2. Run the application:

    ```sh
    mvn spring-boot
3. Access the service:
   The service will be available at http://localhost:8080.
4. Running as an Executable JAR
     ```sh
    mvn clean install
    mvn spring-boot:run
5. Docker
    ```sh
   docker-compose up
6. swagger is exposed at http://localhost:8080/swagger-ui/index.html    
