FROM openjdk:22-jdk
VOLUME /tmp
ADD target/index-svc*SNAPSHOT.jar index-svc.jar
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/index-svc.jar"]